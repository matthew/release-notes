<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE chapter PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
  "https://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
  <!ENTITY % languagedata SYSTEM "language.ent" > %languagedata;
  <!ENTITY % shareddata   SYSTEM "../release-notes.ent" > %shareddata;
]>

<chapter id="ch-whats-new" lang="en">
<title>What's new in &debian; &release;</title>
<para>
  The <ulink url="&url-wiki-newinrelease;">Wiki</ulink> has more information
about this topic.
</para>

<!--
Sources for architecture status:
  https://release.debian.org/buster/arch_qualify.html

Some descriptions of the ports: https://www.debian.org/ports/
-->
<section>
<title>Supported architectures</title>

<para>
The following are the officially supported architectures for &debian;
&release;:
</para>
<itemizedlist>
<listitem>
<para>
32-bit PC (<literal>i386</literal>) and 64-bit PC (<literal>amd64</literal>)
</para>
</listitem>
<listitem>
<para>
64-bit ARM (<literal>arm64</literal>)
</para>
</listitem>
<listitem>
<para>
ARM EABI (<literal>armel</literal>)
</para>
</listitem>
<listitem>
<para>
ARMv7 (EABI hard-float ABI, <literal>armhf</literal>)
</para>
</listitem>
<listitem>
<para>
little-endian MIPS (<literal>mipsel</literal>)
</para>
</listitem>
<listitem>
<para>
64-bit little-endian MIPS (<literal>mips64el</literal>)
</para>
</listitem>
<listitem>
<para>
64-bit little-endian PowerPC (<literal>ppc64el</literal>)
</para>
</listitem>
<listitem>
<para>
IBM System z (<literal>s390x</literal>)
</para>
</listitem>
</itemizedlist>

<para>
You can read more about port status, and port-specific information for your
architecture at the <ulink url="&url-ports;">Debian port
web pages</ulink>.
</para>

</section>

<section id="newdistro">
<title>What's new in the distribution?</title>

<programlisting condition="fixme">
 TODO: Make sure you update the numbers in the .ent file
     using the changes-release.pl script found under ../
</programlisting>

<para>
This new release of Debian again comes with a lot more software than
its predecessor &oldreleasename;; the distribution includes over
&packages-new; new packages, for a total of over &packages-total;
packages.  Most of the software in the distribution has been updated:
over &packages-updated; software packages (this is
&packages-update-percent;% of all packages in &oldreleasename;).
Also, a significant number of packages (over &packages-removed;,
&packages-removed-percent;% of the packages in &oldreleasename;) have
for various reasons been removed from the distribution.  You will not
see any updates for these packages and they will be marked as
"obsolete" in package management front-ends; see <xref
linkend="obsolete"/>.
</para>

<section id="major-packages">
  <title>Desktops and well known packages</title>
<para>
  &debian; again ships with several desktop applications and
  environments.  Among others it now includes the desktop environments
  GNOME<indexterm><primary>GNOME</primary></indexterm> 3.38,
  KDE Plasma<indexterm><primary>KDE</primary></indexterm> 5.20,
  LXDE<indexterm><primary>LXDE</primary></indexterm> 11,
  LXQt<indexterm><primary>LXQt</primary></indexterm> 0.16,
  MATE<indexterm><primary>MATE</primary></indexterm> 1.24, and
  Xfce<indexterm><primary>Xfce</primary></indexterm> 4.16.
</para>
<para>
  Productivity applications have also been upgraded, including the
  office suites:
</para>
  <itemizedlist>
  <listitem>
    <para>
      LibreOffice<indexterm><primary>LibreOffice</primary></indexterm>
      is upgraded to version 7.0;
    </para>
  </listitem>
  <listitem>
    <para>
      Calligra<indexterm><primary>Calligra</primary></indexterm>
      is upgraded to 3.2.
    </para>
  </listitem>
  <listitem>
    <para>
      GNUcash<indexterm><primary>GNUcash</primary></indexterm> is upgraded to 4.4;
    </para>
  </listitem>
  <!-- no updates?
  <listitem>
    <para>
      GNUmeric<indexterm><primary>GNUmeric</primary></indexterm> is upgraded to 1.12;
    </para>
  </listitem>
  <listitem>
    <para>
      Abiword<indexterm><primary>Abiword</primary></indexterm> is upgraded to 3.0.
      </listitem>
      </ -->
  </itemizedlist>

<!-- JFS:
Might it be useful point to https://distrowatch.com/table.php?distribution=debian ?
This provides a more comprehensive comparison among different releases -->

<para>
Among many others, this release also includes the following software updates:
</para>
<informaltable pgwide="1">
  <tgroup cols="3">
    <colspec align="justify"/>
    <colspec align="justify"/>
    <colspec align="justify"/>
    <!-- colspec align="justify" colwidth="3*"/ -->
    <thead>
      <row>
	<entry>Package</entry>
	<entry>Version in &oldrelease; (&oldreleasename;)</entry>
	<entry>Version in &release; (&releasename;)</entry>
      </row>
    </thead>
    <tbody>
      <row id="new-apache2">
	<entry>Apache<indexterm><primary>Apache</primary></indexterm></entry>
	<entry>2.4.38</entry>
	<entry>2.4.46</entry>
      </row>
      <row id="new-bind9">
	<entry>BIND<indexterm><primary>BIND</primary></indexterm> <acronym>DNS</acronym> Server</entry>
	<entry>9.11</entry>
	<entry>9.16</entry>
      </row>
<!--
      <row id="new-chromium">
	<entry>Chromium<indexterm><primary>Chromium</primary></indexterm></entry>
	<entry>53.0</entry>
	<entry>73.0</entry>
      </row>
      <row id="new-courier">
	<entry>Courier<indexterm><primary>Courier</primary></indexterm> <acronym>MTA</acronym></entry>
	<entry>0.73</entry>
	<entry>1.0</entry>
      </row>
-->
      <row id="new-cryptsetup">
        <entry>Cryptsetup<indexterm><primary>Cryptsetup</primary></indexterm></entry>
	<entry>2.1</entry>
	<entry>2.3</entry>
      </row>
<!--
      <row id="new-dia">
	<entry>Dia<indexterm><primary>Dia</primary></indexterm></entry>
	<entry>0.97.2</entry>
	<entry>0.97.3</entry>
        </row>
-->
      <row id="new-dovecot">
	<entry>Dovecot<indexterm><primary>Dovecot</primary></indexterm> <acronym>MTA</acronym></entry>
	<entry>2.3.4</entry>
	<entry>2.3.13</entry>
      </row>
      <row id="new-emacs">
	<entry>Emacs</entry>
	<entry>26.1</entry>
	<entry>27.1</entry>
      </row>
      <row id="new-exim4">
	<entry>Exim<indexterm><primary>Exim</primary></indexterm> default e-mail server</entry>
	<entry>4.92</entry>
	<entry>4.94</entry>
      </row>
<!--
      <row id="new-firefox">
	<entry>Firefox<indexterm><primary>Firefox</primary></indexterm></entry>
	<entry>45.5 (AKA Iceweasel)</entry>
	<entry>60.7 (ESR)</entry>
      </row>
-->
      <row id="new-gcc">
	<entry><acronym>GNU</acronym> Compiler Collection as default compiler<indexterm><primary>GCC</primary></indexterm></entry>
	<entry>8.3</entry>
	<entry>10.2</entry>
      </row>
      <row id="new-gimp">
	<entry><acronym>GIMP</acronym><indexterm><primary>GIMP</primary></indexterm></entry>
	<entry>2.10.8</entry>
	<entry>2.10.22</entry>
      </row>
      <row id="new-gnupg">
	<entry>GnuPG<indexterm><primary>GnuPG</primary></indexterm></entry>
	<entry>2.2.12</entry>
	<entry>2.2.20</entry>
      </row>
      <row id="new-inkscape">
	<entry>Inkscape<indexterm><primary>Inkscape</primary></indexterm></entry>
	<entry>0.92.4</entry>
	<entry>1.0.2</entry>
      </row>
      <row id="new-libc6">
	<entry>the <acronym>GNU</acronym> C library</entry>
	<entry>2.28</entry>
	<entry>2.31</entry>
      </row>
      <row id="new-lighttpd">
	<entry>lighttpd</entry>
	<entry>1.4.53</entry>
	<entry>1.4.59</entry>
      </row>
      <row id="new-linux-image">
        <entry>Linux kernel image</entry>
        <entry>4.19 series</entry>
        <entry>5.10 series</entry>
      </row>
      <row id="llvm-toolchain">
        <entry>LLVM/Clang toolchain</entry>
        <entry>6.0.1 and 7.0.1 (default)</entry>
        <entry>9.0.1 and 11.0.1 (default)</entry>
      </row>
      <row id="new-mariadb">
	<entry>MariaDB<indexterm><primary>MariaDB</primary></indexterm></entry>
	<entry>10.3</entry>
	<entry>10.5</entry>
      </row>
      <row id="new-nginx">
	<entry>Nginx<indexterm><primary>Nginx</primary></indexterm></entry>
	<entry>1.14</entry>
	<entry>1.18</entry>
      </row>
<!--
      <row id="new-openldap">
	<entry>OpenLDAP</entry>
	<entry>2.4.44</entry>
	<entry>2.4.47</entry>
      </row>
-->
      <row id="new-openjdk">
	<entry>OpenJDK<indexterm><primary>OpenJDK</primary></indexterm></entry>
	<entry>11</entry>
	<entry>11</entry>
      </row>
      <row id="new-openssh">
	<entry>OpenSSH<indexterm><primary>OpenSSH</primary></indexterm></entry>
	<entry>7.9p1</entry>
	<entry>8.4p1</entry>
      </row>
      <row id="new-perl">
	<entry>Perl<indexterm><primary>Perl</primary></indexterm></entry>
	<entry>5.28</entry>
	<entry>5.32</entry>
      </row>
      <row id="new-php">
	<entry><acronym>PHP</acronym><indexterm><primary>PHP</primary></indexterm></entry>
	<entry>7.3</entry>
	<entry>7.4</entry>
      </row>
      <row id="new-postfix">
	<entry>Postfix<indexterm><primary>Postfix</primary></indexterm> <acronym>MTA</acronym></entry>
	<entry>3.4</entry>
	<entry>3.5</entry>
      </row>
      <row id="new-postgresql">
	<entry>PostgreSQL<indexterm><primary>PostgreSQL</primary></indexterm></entry>
	<entry>11</entry>
	<entry>13</entry>
      </row>
<!--
      <row id="new-python">
	<entry>Python</entry>
	<entry>2.6</entry>
	<entry>2.7</entry>
      </row>
-->
      <row id="new-python3">
	<entry>Python 3</entry>
	<entry>3.7.3</entry>
	<entry>3.9.1</entry>
      </row>
      <row id="new-rustc">
	<entry>Rustc</entry>
	<entry>1.41 (1.34 for <literal>armel</literal>)</entry>
	<entry>1.48</entry>
      </row>
      <row id="new-samba">
	<entry>Samba</entry>
	<entry>4.9</entry>
	<entry>4.13</entry>
      </row>
      <row id="new-vim">
	<entry>Vim</entry>
	<entry>8.1</entry>
	<entry>8.2</entry>
      </row>
    </tbody>
  </tgroup>
</informaltable>
</section>

<section id="driverless-operation">
  <title>Driverless scanning and printing</title>
  <para>
    Both printing with <literal>CUPS</literal> and scanning with
    <literal>SANE</literal> are increasingly likely to be possible
    without the need for any driver (often non-free) specific
    to the model of the hardware, especially in the case of devices
    marketed in the past five years or so.
  </para>

  <section id="CUPS-and-driverless-printing">
    <title>CUPS and driverless printing</title>
    <para>
     Modern printers connected by ethernet or wireless can already use
     <ulink url="https://wiki.debian.org/CUPSQuickPrintQueues">driverless
     printing</ulink>, implemented via <literal>CUPS</literal> and <systemitem
     role="package">cups-filters</systemitem>, as was described in the <ulink
     url="https://www.debian.org/releases/buster/amd64/release-notes/ch-whats-new.html#driverless-printing">Release
     Notes for buster</ulink>. Debian 11 <quote>bullseye</quote>
     brings the new package <systemitem role="package">ipp-usb</systemitem>,
     which is recommended by <systemitem role="package">cups-daemon</systemitem>
     and uses the vendor-neutral <ulink
     url="https://wiki.debian.org/CUPSDriverlessPrinting#ippoverusb">IPP-over-USB</ulink>
     protocol supported by many modern printers. This allows a USB
     device to be treated as a network device, extending driverless printing
     to include USB-connected printers. The specifics are outlined
     <ulink url="https://wiki.debian.org/CUPSDriverlessPrinting#ipp-usb">on
     the wiki</ulink>.
    </para>
    <para>
     The systemd service file included in the <systemitem
     role="package">ipp-usb</systemitem> package starts the
     <literal>ipp-usb</literal> daemon when a USB-connected
     printer is plugged in, thus making it available to print to. By
     default <systemitem role="package">cups-browsed</systemitem> should
     configure it automatically, or it can be
     <ulink url="https://wiki.debian.org/SystemPrinting">manually set
     up with a local driverless print queue</ulink>.
    </para>
  </section>

  <section id="SANE-and-driverless-scanning">
    <title>SANE and driverless scanning</title>
    <para>
      The official <literal>SANE</literal> driverless backend is
      provided by <literal>sane-escl</literal> in <systemitem
      role="package">libsane1</systemitem>. An independently developed
      driverless backend is <systemitem
      role="package">sane-airscan</systemitem>. Both backends understand
      the <ulink url="https://wiki.debian.org/SaneOverNetwork#escl">eSCL
      protocol</ulink> but <systemitem
      role="package">sane-airscan</systemitem> can also use the <ulink
      url="https://wiki.debian.org/SaneOverNetwork#wsd">WSD</ulink>
      protocol. Users should consider having both backends on their
      systems.
    </para>
    <para>
      <literal>eSCL</literal> and <literal>WSD</literal> are network
      protocols. Consequently they will operate over a USB connection if
      the device is an <literal>IPP-over-USB</literal> device (see
      above). Note that <systemitem
      role="package">libsane1</systemitem> has <systemitem
      role="package">ipp-usb</systemitem> as a recommended package. This
      leads to a suitable device being automatically set up to use a
      driverless backend driver when it is connected to a USB port.
    </para>
   </section>

</section>

<section id="open-command">
  <title>New generic open command</title>
  <para>
    A new <command>open</command> command is available as a
    convenience alias to <command>xdg-open</command> (by default)
    or <command>run-mailcap</command>, managed by the <ulink
    url="&url-man;/bullseye/dpkg/update-alternatives.1.html">update-alternatives(1)</ulink>
    system. It is intended for interactive use at the command line,
    to open files with their default application, which can be a
    graphical program when available.
  </para>
</section>

<section id="cgroupv2">
  <title>Control groups v2</title>
  <para>
    In bullseye, systemd defaults to using control groups v2
    (cgroupv2), which provides a unified resource-control hierarchy.
    Kernel commandline parameters are available to re-enable the legacy
    cgroups if necessary; see the notes for OpenStack in <xref
    linkend="openstack-cgroups"/> section.
  </para>
</section>

<section id="persistent-journal">
  <title>Persistent systemd journal</title>
  <para>
    Systemd in bullseye activates its persistent journal functionality
    by default, storing its files in
    <filename>/var/log/journal/</filename>. See <ulink
    url="&url-man;/bullseye/systemd/systemd-journald.service.8.html">systemd-journald.service(8)</ulink>
    for details; note that on Debian the journal is readable for
    members of <literal>adm</literal>, in addition to the default
    <literal>systemd-journal</literal> group.
  </para>
  <para>
    This should not interfere with any existing traditional logging
    daemon such as <systemitem role="package">rsyslog</systemitem>,
    but users who are not relying on special features of such a daemon
    may wish to uninstall it and switch over to using only the
    journal.
  </para>
</section>

<section id="fcitx5">
  <title>New Fcitx 5 Input Method</title>
  <para>
    Fcitx 5 is an input method for Chinese, Japanese, Korean and many
    other languages. It is the successor of the popular Fcitx 4 in
    buster. The new version supports Wayland and has better addon
    support. More information including the migration guide can be
    found <ulink url="https://wiki.debian.org/I18n/Fcitx5">on the
    wiki</ulink>.
  </para>
</section>

<section id="debian-med">
  <title>News from Debian Med Blend</title>
    <para>
      The Debian Med team has been taking part in the fight against
      <literal>COVID-19</literal> by packaging software for
      researching the virus on the sequence level and for fighting the
      pandemic with the tools used in epidemiology. The effort will be
      continued in the next release cycle with focus on machine
      learning tools that are used in both fields.
    </para>
    <para>
      Besides the addition of new packages in the field of life
      sciences and medicine, more and more existing packages have
      gained Continuous Integration support.
    </para>
    <para>
      A range of performance critical applications now benefit from
      <ulink url="https://wiki.debian.org/SIMDEverywhere">SIMD
      Everywhere</ulink>. This library allows packages to be available
      on more hardware platforms supported by Debian (notably on
      <literal>arm64</literal>) while maintaining the performance
      benefit brought by processors supporting vector extensions, such
      as <literal>AVX</literal> on <literal>amd64</literal>, or
      <literal>NEON</literal> on <literal>arm64</literal>.
    </para>
    <para>
      To install packages maintained by the Debian Med team, install
      the metapackages named
      <literal>med-<replaceable>*</replaceable></literal>, which are
      at version 3.6.x for Debian bullseye. Feel free to visit the
      <ulink url="https://blends.debian.org/med/tasks">Debian Med
      tasks pages</ulink> to see the full range of biological and
      medical software available in Debian.
    </para>
</section>

<section id="exfat-in-linux-kernel">
  <title>Kernel support for exFAT</title>
    <para>
      bullseye is the first release providing a Linux kernel which has
      support for the exFAT filesystem, and defaults to using it for mounting
      exFAT filesystems. Consequently it's no longer required to use the
      filesystem-in-userspace implementation provided via the
      <systemitem role="package">exfat-fuse</systemitem> package. If you
      would like to continue to use the filesystem-in-userspace
      implementation, you need to invoke the
      <command>mount.exfat-fuse</command> helper directly when mounting an
      exFAT filesystem.
    </para>
    <para>
      Tools for creating and checking an exFAT filesystem are provided in the
      <systemitem role="package">exfatprogs</systemitem> package by the
      authors of the Linux kernel exFAT implementation. The independent
      implementation of those tools provided via the existing
      <systemitem role="package">exfat-utils</systemitem> package is still
      available, but cannot be co-installed with the new implementation. It's
      recommended to migrate to the
      <systemitem role="package">exfatprogs</systemitem> package, though you
      must take care of command options, which are most likely incompatible.
    </para>
</section>

<section id="man-xx">
  <!-- buster to bullseye -->
  <title>Improved man page translations</title>
  <para>
    The manual pages for several projects such as systemd, util-linux,
    OpenSSH, and Mutt in a number of languages, including French,
    Spanish, and Macedonian, have been substantially improved. To
    benefit from this, please install
    <literal>manpages-<replaceable>xx</replaceable></literal> (where
    <literal><replaceable>xx</replaceable></literal> is the code for
    your preferred natural language).
  </para>
  <para>
    During the lifetime of the bullseye release, backports of further
    translation improvements will be provided via the
    <literal>backports</literal> archive.
  </para>
</section>

</section>
</chapter>
