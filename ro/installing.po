# Romanian translation of the Debian Release Notes
# This file is distributed under the same license as the Debian Release Notes.
# Copyright (C) 2005-2019 the respective copyright holders:
#
# Dan Damian <dand@codemonkey.ro>, 2005.
# Eddy Petrișor <eddy.petrisor@gmail.com>, 2006, 2007, 2009.
# Stan Ioan-Eugen <stan.ieugen@gmail.com>, 2006, 2007.
# Andrei Popescu <andreimpopescu@gmail.com>, 2007, 2008, 2009, 2012, 2013, 2019.
# Igor Știrbu <igor.stirbu@gmail.com>, 2009.
# ioan-eugen STAN <stan.ieugen@gmail.com>, 2011.
# Sorin-Mihai Vârgolici <smv@yobicore.org>, 2011.
# Daniel Șerbănescu <daniel [at] serbanescu [dot] dk>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: installing\n"
"POT-Creation-Date: 2021-03-27 22:34+0100\n"
"PO-Revision-Date: 2019-07-06 18:17+0300\n"
"Last-Translator: Daniel Șerbănescu <daniel [at] serbanescu [dot] dk>\n"
"Language-Team: Romanian Team <debian-l10n-romanian@lists.debian.org>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.2.1\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n==0 || (n!=1 && n%100>=1 && n"
"%100<=19) ? 1 : 2);\n"
"X-Poedit-SourceCharset: UTF-8\n"

#. type: Attribute 'lang' of: <chapter>
#: en/installing.dbk:8
msgid "en"
msgstr "ro"

#. type: Content of: <chapter><title>
#: en/installing.dbk:9
msgid "Installation System"
msgstr "Sistemul de instalare"

#. type: Content of: <chapter><para>
#: en/installing.dbk:11
msgid ""
"The Debian Installer is the official installation system for Debian.  It "
"offers a variety of installation methods.  Which methods are available to "
"install your system depends on your architecture."
msgstr ""
"Programul de instalare Debian este sistemul de instalare oficial pentru "
"Debian. Acesta oferă o varietate de metode de instalare. Metodele "
"disponibile pentru instalarea sistemului depind de arhitectura sistemului "
"dumneavoastră."

#. type: Content of: <chapter><para>
#: en/installing.dbk:16
msgid ""
"Images of the installer for &releasename; can be found together with the "
"Installation Guide on the <ulink url=\"&url-installer;\">Debian website</"
"ulink>."
msgstr ""
"Imaginile programului de instalare pentru &releasename; pot fi găsite "
"împreună cu Ghidul de instalare pe <ulink url=\"&url-installer;\">situl "
"Debian</ulink>."

#. type: Content of: <chapter><para>
#: en/installing.dbk:21
msgid ""
"The Installation Guide is also included on the first media of the official "
"Debian DVD (CD/blu-ray) sets, at:"
msgstr ""
"Ghidul de instalare mai este inclus și pe primul disc din seturile oficiale "
"de DVD Debian (CD/blu-ray) la:"

#. type: Content of: <chapter><screen>
#: en/installing.dbk:25
#, no-wrap
msgid "/doc/install/manual/<replaceable>language</replaceable>/index.html\n"
msgstr "/doc/install/manual/<replaceable>limbă</replaceable>/index.html\n"

#. type: Content of: <chapter><para>
#: en/installing.dbk:28
msgid ""
"You may also want to check the <ulink url=\"&url-installer;index#errata"
"\">errata</ulink> for debian-installer for a list of known issues."
msgstr ""
"Ar fi bine să verificați și <ulink url=\"&url-installer;index#errata"
"\">erata</ulink> debian-installer (n. trad. Programul de instalare Debian) "
"pentru o listă a problemelor cunoscute."

#. type: Content of: <chapter><section><title>
#: en/installing.dbk:33
msgid "What's new in the installation system?"
msgstr "Ce este nou în sistemul de instalare?"

#. type: Content of: <chapter><section><para>
#: en/installing.dbk:35
msgid ""
"There has been a lot of development on the Debian Installer since its "
"previous official release with &debian; &oldrelease;, resulting in improved "
"hardware support and some exciting new features or improvements."
msgstr ""
"Programul de instalare Debian a avut parte de multe schimbări de la "
"versiunea precedentă lansată oficial odată cu &debian; &oldrelease;, "
"rezultând atât în suport mai bun pentru hardware cât și capabilități noi, "
"interesante."

#. type: Content of: <chapter><section><para>
#: en/installing.dbk:41
msgid ""
"If you are interested in an overview of the detailed changes since "
"&oldreleasename;, please check the release announcements for the "
"&releasename; beta and RC releases available from the Debian Installer's "
"<ulink url=\"&url-installer-news;\">news history</ulink>."
msgstr ""
"Dacă vă interesează o vedere de ansamblu a modificărilor detaliate începând "
"cu &oldreleasename; consultați anunțurile de lansare pentru versiunile "
"&releasename; beta și RC, disponibile la <ulink url=\"&url-installer-news;"
"\">istoricul știrilor</ulink> Programului de instalare Debian."

#. type: Content of: <chapter><section><section><title>
#: en/installing.dbk:120
msgid "Automated installation"
msgstr "Instalarea automată"

#. type: Content of: <chapter><section><section><para>
#: en/installing.dbk:122
msgid ""
"Some changes mentioned in the previous section also imply changes in the "
"support in the installer for automated installation using preconfiguration "
"files.  This means that if you have existing preconfiguration files that "
"worked with the &oldreleasename; installer, you cannot expect these to work "
"with the new installer without modification."
msgstr ""
"Unele dintre modificările menționate în secțiunea precedentă implică și "
"modificări în suportul pentru instalări automate folosind fișiere de "
"preconfigurare. Acest lucru înseamnă că dacă aveți fișiere de preconfigurare "
"care au funcționat cu programul de instalare din &oldreleasename; nu vă "
"așteptați ca acestea să funcționeze fără modificări cu noul program de "
"instalare."

#. type: Content of: <chapter><section><section><para>
#: en/installing.dbk:129
msgid ""
"The <ulink url=\"&url-install-manual;\">Installation Guide</ulink> has an "
"updated separate appendix with extensive documentation on using "
"preconfiguration."
msgstr ""
"<ulink url=\"&url-install-manual;\">Ghidul de instalare</ulink> conține acum "
"o anexă separată și actualizată cu documentație amplă despre folosirea "
"preconfigurărilor."

#. type: Content of: <chapter><section><title>
#: en/installing.dbk:138
#, fuzzy
#| msgid "Automated installation"
msgid "Cloud installations"
msgstr "Instalarea automată"

#. type: Content of: <chapter><section><para>
#: en/installing.dbk:140
msgid ""
"The <ulink url=\"&url-cloud-team;\">cloud team</ulink> publishes Debian "
"bullseye for several popular cloud computing services including:"
msgstr ""

#. type: Content of: <chapter><section><para><itemizedlist><listitem><para>
#: en/installing.dbk:147
msgid "OpenStack"
msgstr ""

#. type: Content of: <chapter><section><para><itemizedlist><listitem><para>
#: en/installing.dbk:152
msgid "Amazon Web Services"
msgstr ""

#. type: Content of: <chapter><section><para><itemizedlist><listitem><para>
#: en/installing.dbk:157
msgid "Microsoft Azure"
msgstr ""

#. type: Content of: <chapter><section><para>
#: en/installing.dbk:164
msgid ""
"Cloud images provide automation hooks via cloud-init and prioritize fast "
"instance startup using specifically optimized kernel packages and grub "
"configurations.  Images supporting different architectures are provided "
"where appropriate and the cloud team endeavors to support all features "
"offered by the cloud service."
msgstr ""

#. type: Content of: <chapter><section><para>
#: en/installing.dbk:173
msgid ""
"More details are available at <ulink url=\"&url-cloud;\">cloud.debian.org</"
"ulink> and <ulink url=\"&url-cloud-wiki;\">on the wiki</ulink>."
msgstr ""

#. type: Content of: <chapter><section><title>
#: en/installing.dbk:180
msgid "Container and Virtual Machine images"
msgstr ""

#. type: Content of: <chapter><section><para>
#: en/installing.dbk:182
msgid ""
"Multi-architecture Debian bullseye container images are available on <ulink "
"url=\"&url-docker-hub;\">Docker Hub</ulink>.  In addition to the standard "
"images, a <quote>slim</quote> variant is available that reduces disk usage."
msgstr ""

#. type: Content of: <chapter><section><para>
#: en/installing.dbk:188
msgid ""
"Virtual machine images for the Hashicorp Vagrant VM manager are published to "
"<ulink url=\"&url-vagrant-cloud;\">Vagrant Cloud</ulink>."
msgstr ""

#~ msgid ""
#~ "Most notably there is the initial support for UEFI Secure Boot (see <xref "
#~ "linkend=\"secure-boot\"/>), which has been added to the installation "
#~ "images."
#~ msgstr ""
#~ "Cel mai important de notat este suportul inițial pentru UEFI Secure Boot "
#~ "(consultați <xref linkend=\"secure-boot\"/>), care a fost adăugat la "
#~ "imaginile de instalare."

#~ msgid "Major changes"
#~ msgstr "Schimbări majore"

#~ msgid "New languages"
#~ msgstr "Limbi noi"

#~ msgid ""
#~ "Thanks to the huge efforts of translators, &debian; can now be installed "
#~ "in 75 languages, including English.  Most languages are available in both "
#~ "the text-based installation user interface and the graphical user "
#~ "interface, while some are only available in the graphical user interface."
#~ msgstr ""
#~ "Mulțumită eforturilor uriașe ale traducătorilor, &debian; poate fi "
#~ "instalat acum în 75 de limbi, inclusiv engleza. Majoritatea limbilor sunt "
#~ "disponibile atât la instalarea în mod text cât și la cea cu interfață "
#~ "grafică, în timp ce unele sunt disponibile doar la instalarea cu "
#~ "interfață grafică."

#~ msgid ""
#~ "The languages that can only be selected using the graphical installer as "
#~ "their character sets cannot be presented in a non-graphical environment "
#~ "are: Amharic, Bengali, Dzongkha, Gujarati, Hindi, Georgian, Kannada, "
#~ "Khmer, Malayalam, Marathi, Nepali, Punjabi, Tamil, Telugu, Tibetan, and "
#~ "Uyghur."
#~ msgstr ""
#~ "Limbile care pot fi selectate doar folosind modul de instalare grafic, "
#~ "deoarece caracterele lor specifice nu pot fi prezentate într-un mediu non-"
#~ "grafic, sunt: amharică, bengali, dzongkha, gujarati, hindusă, georgiană, "
#~ "khannadă, khmeră, malayalam, marathi, nepaleză, punjabi, tamilă, telugu, "
#~ "tibetană și uyghur."

#~ msgid "Removed ports"
#~ msgstr "Portări eliminate"

#~ msgid ""
#~ "Support for the <literal>powerpc</literal> architecture has been removed."
#~ msgstr ""
#~ "Suportul pentru arhitectura <literal>powerpc</literal> a fost eliminat."

#~ msgid "New ports"
#~ msgstr "Portări noi"

#~ msgid ""
#~ "Support for the <literal>mips64el</literal> architecture has been added "
#~ "to the installer."
#~ msgstr ""
#~ "Suport pentru arhitectura <literal>mips64el</literal> a fost adăugat la "
#~ "programul de instalare."

#~ msgid "Graphical installer"
#~ msgstr "Programul de instalare grafic"

#~ msgid ""
#~ "The graphical installer is now the default on supported platforms.  The "
#~ "text installer is still accessible from the very first menu, or if the "
#~ "system has limited capabilities."
#~ msgstr ""
#~ "Programul de instalare grafic este acum implicit pe platformele "
#~ "suportate. Programul de instalare bazat pe text este încă accesibil din "
#~ "primul meniu, sau dacă sistemul are capacități limitate"

#~ msgid "The kernel flavor has been bumped to <literal>i686</literal>"
#~ msgstr "Varianta nucleului a fost ridicată la <literal>i686</literal>"

#~ msgid ""
#~ "The kernel flavor <literal>i586</literal> has been renamed to "
#~ "<literal>i686</literal>, since <literal>i586</literal> is no longer "
#~ "supported."
#~ msgstr ""
#~ "Varianta nucleului <literal>i586</literal> a fost redenumită în "
#~ "<literal>i686</literal>, pentru că <literal>i586</literal> nu mai este "
#~ "suportată."

#~ msgid "Desktop selection"
#~ msgstr "Selecția pentru desktop"

#~ msgid ""
#~ "Since jessie, the desktop can be chosen within tasksel during "
#~ "installation, and several desktops can be selected at the same time."
#~ msgstr ""
#~ "De la jessie, desktopul poate fi ales din tasksel în timpul instalării, "
#~ "și câteva desktopuri pot fi selectate simultan."

#~ msgid "UEFI boot"
#~ msgstr "Inițializare UEFI"

#~ msgid "Note that this does not include support for UEFI Secure Boot."
#~ msgstr "Totuși nu este inclus suport pentru UEFI Secure Boot."

#~ msgid "Software speech support"
#~ msgstr "Suport pentru sinteză vocală"

#~ msgid ""
#~ "&debian; can be installed using software speech, for instance by visually "
#~ "impaired people who do not use a Braille device.  This is triggered "
#~ "simply by typing <literal>s</literal> and <literal>Enter</literal> at the "
#~ "installer boot beep.  More than a dozen languages are supported."
#~ msgstr ""
#~ "&debian; poate fi instalat folosind o sinteză vocală, de exemplu de către "
#~ "persoane nevăzătoare care nu folosesc un dispozitiv Braille. Această "
#~ "facilitate este declanșată apăsând tastele <literal>s</literal> și "
#~ "<literal>Enter</literal> la semnalul sonor al programului de instalare. "
#~ "Sunt suportate mai multe limbi."

#~ msgid "New supported platforms"
#~ msgstr "Platforme nou suportate"

#~ msgid "The installation system now supports the following platforms:"
#~ msgstr "Sistemul de instalare suportă acum următoarele platforme:"

#~ msgid "Intel Storage System SS4000-E"
#~ msgstr "Sistemul de stocare Intel SS4000-E"

#~ msgid "Marvell's Kirkwood platform:"
#~ msgstr "Platforma Marvell Kirkwood:"

#~ msgid "QNAP TS-110, TS-119, TS-210, TS-219, TS-219P and TS-419P"
#~ msgstr "QNAP TS-110, TS-119, TS-210, TS-219, TS-219P și TS-419P"

#~ msgid "Marvell SheevaPlug and GuruPlug"
#~ msgstr "Marvell SheevaPlug și GuruPlug"

#~ msgid "Marvell OpenRD-Base, OpenRD-Client and OpenRD-Ultimate"
#~ msgstr "Marvell OpenRD-Base, OpenRD-Client și OpenRD-Ultimate"

#~ msgid "HP t5325 Thin Client (partial support)"
#~ msgstr "HP t5325 Thin Client (suportată parțial)"

#~ msgid "Languages added in this release include:"
#~ msgstr "Limbile adăugate la această lansare includ:"

#~ msgid ""
#~ "Welsh has been re-added to the graphical and text-based installer (it had "
#~ "been removed in &oldreleasename;)."
#~ msgstr ""
#~ "Limba welsh a fost re-adăugată în programul de instalare grafic și cel "
#~ "text (fusese scoasă în &oldreleasename;)."

#~ msgid "Tibetan and Uyghur have been added to the graphical installer."
#~ msgstr ""
#~ "Limbile tibetană și uyghur au fost adăugate la modul grafic de instalare."

#~ msgid "Network configuration"
#~ msgstr "Configurarea rețelei"

#~ msgid "The installer now supports installation on IPv6-only networks."
#~ msgstr ""
#~ "Sistemul de instalare suportă acum instalarea folosind doar rețele IPv6."

#~ msgid "It is now possible to install over a WPA-encrypted wireless network."
#~ msgstr "Este posibilă instalarea folosind o rețea fără fir criptată cu WPA."

#~ msgid "Default filesystem"
#~ msgstr "Sistemul de fișiere implicit"

#~ msgid ""
#~ "<literal>ext4</literal> is the default filesystem for new installations, "
#~ "replacing <literal>ext3</literal>."
#~ msgstr ""
#~ "<literal>ext4</literal> este sistemul de fișiere implicit pentru "
#~ "instalări noi, înlocuind <literal>ext3</literal>."

#~ msgid ""
#~ "The <literal>btrfs</literal> filesystem is provided as a technology "
#~ "preview."
#~ msgstr ""
#~ "Sistemul de fișiere <literal>btrfs</literal> este disponibil ca și pre-"
#~ "vizualizare de tehnologie."

#~ msgid ""
#~ "It is now possible to install PCs in UEFI mode instead of using the "
#~ "legacy BIOS emulation."
#~ msgstr ""
#~ "Acum este posibilă instalarea computerelor de tip PC în modul UEFI în "
#~ "locul emulării BIOS."

#~ msgid ""
#~ "Asturian, Estonian, Icelandic, Kazakh and Persian have been added to the "
#~ "graphical and text-based installer."
#~ msgstr ""
#~ "Limbile asturiană, islandeză, cazacă și persană au fost adăugate la "
#~ "programul de instalare în mod text și grafic."

#~ msgid ""
#~ "Thai, previously available only in the graphical user interface, is now "
#~ "available also in the text-based installation user interface too."
#~ msgstr ""
#~ "Limba thai, disponibilă în trecut doar în modul de instalare grafic , "
#~ "este disponibilă acum și în modul text de instalare."

#~ msgid ""
#~ "Due to the lack of translation updates two languages were dropped in this "
#~ "release: Wolof and Welsh."
#~ msgstr ""
#~ "Datorită lipsei actualizărilor la traduceri, două limbi nu mai sunt "
#~ "disponibile în această versiune: wolof și welsh."

#~ msgid "Dropped platforms"
#~ msgstr "Platforme care nu mai sunt suportate"

#~ msgid ""
#~ "Support for the Alpha ('alpha'), ARM ('arm') and HP PA-RISC ('hppa')  "
#~ "architectures has been dropped from the installer.  The 'arm' "
#~ "architecture is obsoleted by the ARM EABI ('armel') port."
#~ msgstr ""
#~ "Suportul pentru arhitecturile Alpha („alpha”), ARM („arm”) și HP PA-RISC "
#~ "(„hppa”) a fost înlăturat din programul de instalare. Portarea „arm” a "
#~ "fost înlocuită de către arhitectura ARM EABI ('armel')."

#~ msgid "Support for kFreeBSD"
#~ msgstr "Suport pentru kFreeBSD"

#~ msgid ""
#~ "The installer can be used to install the kFreeBSD instead of the Linux "
#~ "kernel and test the technology preview. To use this feature the "
#~ "appropriate installation image (or CD/DVD set) has to be used."
#~ msgstr ""
#~ "Programul de instalare poate fi utilizat pentru a instala kFreeBSD în "
#~ "locul nucleului Linux pentru a testa această previzualizare de "
#~ "tehnologie. Pentru a utiliza această caracteristică trebuie să utilizați "
#~ "imaginea de instalare potrivită (sau setul de CD-uri/DVD-uri)."

#~ msgid "GRUB 2 is the default bootloader"
#~ msgstr "GRUB 2 este încărcătorul de sistem implicit"

#~ msgid ""
#~ "The bootloader that will be installed by default is <systemitem role="
#~ "\"package\">grub-pc</systemitem> (GRUB 2)."
#~ msgstr ""
#~ "Încărcătorul de sistem instalat implicit este <systemitem role=\"package"
#~ "\">grub-pc</systemitem> (GRUB 2)."

#~ msgid "Help during the installation process"
#~ msgstr "Ajutor pe timpul procesului de instalare"

#~ msgid ""
#~ "The dialogs presented during the installation process now provide help "
#~ "information. Although not currently used in all dialogs, this feature "
#~ "would be increasingly used in future releases. This will improve the user "
#~ "experience during the installation process, especially for new users."
#~ msgstr ""
#~ "Dialogurile prezentate pe timpul procesului de instalare oferă acum și "
#~ "informații ajutătoare. Deși caracteristica nu este folosită în toate "
#~ "dialogurile, ea va fi folosită mai mult în lansările viitoare. Aceasta va "
#~ "îmbunătății experiența utilizatorilor pe timpul procesului de instalare, "
#~ "în special pentru utilizatorii noi."

#~ msgid "Installation of Recommended packages"
#~ msgstr "Instalarea pachetelor recomandate"

#~ msgid ""
#~ "The installation system will install all recommended packages by default "
#~ "throughout the process except for some specific situations in which the "
#~ "general setting gives undesired results."
#~ msgstr ""
#~ "Sistemul de instalare va instala implicit toate pachetele recomandate de-"
#~ "a lungul procesului, cu excepția unor situații specifice în care "
#~ "configurarea generală produce rezultate nedorite."

#~ msgid "Automatic installation of hardware-specific packages"
#~ msgstr "Instalarea automată a pachetelor specifice unui anumit echipament"

#~ msgid ""
#~ "The system will automatically select for installation hardware-specific "
#~ "packages when they are appropriate. This is achieved through the use of "
#~ "<literal>discover-pkginstall</literal> from the <systemitem role=\"package"
#~ "\">discover</systemitem> package."
#~ msgstr ""
#~ "Sistemul va alege automat pentru instalare, atunci când este necesar, "
#~ "pachetele specifice unui anumit echipament. Acest lucru este obținut prin "
#~ "intermediul <literal>discover-pkginstall</literal> din pachetul "
#~ "<systemitem role=\"package\">discover</systemitem>."

#~ msgid "Support for installation of previous releases"
#~ msgstr "Suport pentru instalarea versiunilor precedente"

#~ msgid ""
#~ "The installation system can be also used for the installation of previous "
#~ "release, such as &oldreleasename;."
#~ msgstr ""
#~ "Sistemul de instalare poate fi de asemenea utilizat pentru instalarea "
#~ "versiunilor precedente, cum ar fi &oldreleasename;."

#~ msgid "Improved mirror selection"
#~ msgstr "Selecție sit-alternativ îmbunătățită"

#~ msgid ""
#~ "The installation system provides better support for installing both "
#~ "&releasename; as well as &oldreleasename; and older releases (through the "
#~ "use of archive.debian.org). In addition, it will also check that the "
#~ "selected mirror is consistent and holds the selected release."
#~ msgstr ""
#~ "Sistemul de instalare oferă suport mai bun pentru instalarea versiunilor "
#~ "&releasename; și &oldreleasename; dar și a versiunilor mai vechi "
#~ "(folosind archive.debian.org). În plus, va verifica dacă situl alternativ "
#~ "folosit este consecvent și conține versiunea aleasă."

#~ msgid "Changes in partitioning features"
#~ msgstr "Modificări ale caracteristicilor de partiționare"

#~ msgid ""
#~ "This release of the installer supports the use of the ext4 file system "
#~ "and it also simplifies the creation of RAID, LVM and crypto protected "
#~ "partitioning systems. Support for the reiserfs file system is no longer "
#~ "included by default, although it can be optionally loaded."
#~ msgstr ""
#~ "Această versiune a programului de instalare suportă folosirea sistemului "
#~ "de fișiere ext4 și simplifică de asemenea crearea sistemelor de "
#~ "partiționare RAID, LVM și a partițiilor criptate. Suportul pentru "
#~ "sistemul de fișiere reiserfs nu mai este inclus în mod implicit, dar "
#~ "poate fi încărcat opțional."

#~ msgid "Support for loading firmware debs during installation"
#~ msgstr ""
#~ "Suport pentru încărcarea de pachete cu microcod în timpul instalării"

#~ msgid ""
#~ "It is now possible to load firmware package files from the installation "
#~ "media in addition to removable media, allowing the creation of PXE images "
#~ "and CDs/DVDs with included firmware packages."
#~ msgstr ""
#~ "Este posibil acum să încărcați pachete cu microcod de pe mediul de "
#~ "instalare, în plus față de mediile detașabile, permițând crearea de "
#~ "imagini PXE și CD-uri/DVD-uri cu pachete cu microcod incluse. "

#~ msgid ""
#~ "Starting with Debian &release;, non-free firmware has been moved out of "
#~ "main.  To install Debian on hardware that needs non-free firmware, you "
#~ "can either provide the firmware yourself during installation or use pre-"
#~ "made non-free CDs/DVDs which include the firmware. See the <ulink url="
#~ "\"http://www.debian.org/distrib\">Getting Debian section</ulink> on the "
#~ "Debian website for more information."
#~ msgstr ""
#~ "Începând cu lansarea Debian &release;, microcodul proprietar a fost "
#~ "înlăturat din „main”. Pentru a instala Debian pe echipament ce necesită "
#~ "microcod proprietar, fie oferiți dumneavoastră microcodul în timpul "
#~ "procesului de instalare sau folosiți CD-uri/DVD-uri non-libere gata "
#~ "realizate, ce includ acest microcod. Vizitați <ulink url=\"http://www."
#~ "debian.org/distrib\">secțiunea Obțineți Debian</ulink> de pe situl "
#~ "Debian, pentru mai multe informații."

#~ msgid "Improved localisation selection"
#~ msgstr "Selectare simplificată a localizării"

#~ msgid ""
#~ "The selection of localisation-related values (language, location and "
#~ "locale settings) is now less interdependent and more flexible. Users will "
#~ "be able to customize the system to their localisation needs more easily "
#~ "while still make it comfortable to use for users that want to select the "
#~ "locale most common for the country they reside in."
#~ msgstr ""
#~ "Alegerea valorilor ce țin de localizare (limbă, loc și configurări "
#~ "locale) este acum mult mai puțin interdependentă și mult mai flexibilă. "
#~ "Utilizatorii vor putea să personalizeze sistemul după nevoile lor de "
#~ "localizare cu mai multă ușurință, permițând în același timp celorlați "
#~ "utilizatori să aleagă preferințele locale potrivite țării în care "
#~ "locuiesc."

#~ msgid ""
#~ "Additionally, the consequences of localisation choices (such as timezone, "
#~ "keymap and mirror selection) are now more obvious to the user."
#~ msgstr ""
#~ "În plus, consecințele opțiunilor de localizare (zona de timp, harta de "
#~ "taste și situl-alternativ) sunt acum mult mai evidente utilizatorului."

#~ msgid "Live system installation"
#~ msgstr "Instalarea pe baza unui sistem live"

#~ msgid ""
#~ "The installer now supports live systems in two ways. First, an installer "
#~ "included on live system media can use the contents of the live system in "
#~ "place of the regular installation of the base system. Second, the "
#~ "installer may now be launched while running the live system, allowing the "
#~ "user to do other things with the live system during the install. Both "
#~ "features are built into the Debian Live images offered at <ulink url="
#~ "\"http://cdimage.debian.org/\" />."
#~ msgstr ""
#~ "Programul de instalare suportă acum sisteme live în două moduri. Pe de o "
#~ "parte, un program de instalare inclus pe mediul sistemului live poate "
#~ "folosi conținutul din sistemul live în locul instalării obișnuite a "
#~ "sistemului de bază. Pe de altă parte, programul de instalare poate acum "
#~ "să fie pornit în timp ce sistemul live rulează. Ambele moduri de folosire "
#~ "sunt incluse în imaginile Debian Live oferite la <ulink url=\"http://"
#~ "cdimage.debian.org/\" />."
