# Translation of moreinfo.po to Galician
# Copyright (C) 2021 Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# Pablo <parodper@gmail.com>, 2021.
#
# Traductores:
#     Para esta traducción usei (Pablo) o dicionario do Proxecto Trasno (http://termos.trasno.gal/) e o DiGaTIC (http://www.digatic.org/gl)
#
msgid ""
msgstr ""
"Project-Id-Version: release-notes 11\n"
"POT-Creation-Date: 2021-03-16 23:10+0100\n"
"PO-Revision-Date: 2021-06-10 20:58+0200\n"
"Last-Translator: Pablo <parodper@gmail.com>\n"
"Language-Team: Galician <debian-l10n-galician@lists.debian.org>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.4.2\n"

#. type: Attribute 'lang' of: <chapter>
#: en/moreinfo.dbk:8
msgid "en"
msgstr "gl"

#. type: Content of: <chapter><title>
#: en/moreinfo.dbk:9
msgid "More information on &debian;"
msgstr "Máis información en &debian;"

#. type: Content of: <chapter><section><title>
#: en/moreinfo.dbk:11
msgid "Further reading"
msgstr "Lecturas recomendadas"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:13
msgid ""
"Beyond these release notes and the installation guide, further documentation "
"on Debian is available from the Debian Documentation Project (DDP), whose "
"goal is to create high-quality documentation for Debian users and "
"developers, such as the Debian Reference, Debian New Maintainers Guide, the "
"Debian FAQ, and many more.  For full details of the existing resources see "
"the <ulink url=\"&url-ddp;\">Debian Documentation website</ulink> and the "
"<ulink url=\"&url-wiki;\">Debian Wiki</ulink>."
msgstr ""
"Ademais destas notas de versión e da guía de instalación, hai máis "
"documentación sobre Debian dispoñible no Proxecto de Documentación Debian "
"(DDP en inglés), cuxos obxectivos son a creación de documentación de alta "
"calidade para os usuarios e desenvolvedores de Debian, como por exemplo a "
"Referencia de Debian, a Nova Guía para os Mantedores de Debian, as Preguntas "
"Frecuentes en Debian, e moitas máis.  Para máis detalles sobre os recursos "
"existentes consulte a <ulink url=\"&url-ddp;\">páxina web da Documentación "
"de Debian</ulink> e a <ulink url=\"&url-wiki;\">Wiki Debian</ulink>."

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:23
msgid ""
"Documentation for individual packages is installed into <filename>/usr/share/"
"doc/<replaceable>package</replaceable></filename>.  This may include "
"copyright information, Debian specific details, and any upstream "
"documentation."
msgstr ""
"A documentación para os paquetes individuais instálase en <filename>/usr/"
"share/doc/<replaceable>nome-paquete</replaceable></filename>.  Esto pode "
"incluír información de dereitos de autor, detalles específicos de Debian, e "
"calquera documentación orixinal."

#. type: Content of: <chapter><section><title>
#: en/moreinfo.dbk:31
msgid "Getting help"
msgstr "Obter axuda"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:33
msgid ""
"There are many sources of help, advice, and support for Debian users, though "
"these should only be considered after researching the issue in available "
"documentation.  This section provides a short introduction to these sources "
"which may be helpful for new Debian users."
msgstr ""
"Hai moitas fontes de axuda, consellos e asistencia técnica para os usuarios "
"de Debian, inda que estas só se deberían consultar despois de investigar o "
"problema na documentación dispoñible.  Esta sección contén unha corta "
"introdución a estas fontes que poden ser de axuda para os novos usuarios de "
"Debian."

#. type: Content of: <chapter><section><section><title>
#: en/moreinfo.dbk:39
msgid "Mailing lists"
msgstr "Listas de correo"

#. type: Content of: <chapter><section><section><para>
#: en/moreinfo.dbk:41
msgid ""
"The mailing lists of most interest to Debian users are the debian-user list "
"(English) and other debian-user-<replaceable>language</replaceable> lists "
"(for other languages).  For information on these lists and details of how to "
"subscribe see <ulink url=\"&url-debian-list-archives;\"></ulink>.  Please "
"check the archives for answers to your question prior to posting and also "
"adhere to standard list etiquette."
msgstr ""
"As listas de correo máis importantes para os usuarios de Debian son as "
"listas debian-user (en inglés) e as outras debian-user-<replaceable>idioma</"
"replaceable> (para os outros idiomas).  Para máis información sobre estas "
"listas e detalles de como subscribirse consulte <ulink url=\"&url-debian-"
"list-archives;\"></ulink>.  Por favor rebusca nos arquivos para ver se xa "
"existen respostas á túa pregunta e segue as normas de conduta da lista."

#. type: Content of: <chapter><section><section><title>
#: en/moreinfo.dbk:51
msgid "Internet Relay Chat"
msgstr "Internet Relay Chat (IRC)"

#. type: Content of: <chapter><section><section><para>
#: en/moreinfo.dbk:53
msgid ""
"Debian has an IRC channel dedicated to support and aid for Debian users, "
"located on the OFTC IRC network.  To access the channel, point your favorite "
"IRC client at irc.debian.org and join <literal>#debian</literal>."
msgstr ""
"Debian ten unha canle IRC dedicada a dar soporte técnico e axudar aos "
"usuarios de Debian, situada na rede IRC OFTC.  Para acceder a esa canle "
"conéctese co seu cliente IRC a irc.debian.org e únase a <literal>#debian</"
"literal>."

#. type: Content of: <chapter><section><section><para>
#: en/moreinfo.dbk:58
msgid ""
"Please follow the channel guidelines, respecting other users fully.  The "
"guidelines are available at the <ulink url=\"&url-wiki;DebianIRC\">Debian "
"Wiki</ulink>."
msgstr ""
"Por favor siga as normas da canle, e respecte aos outros usuarios.  Pode "
"consultar as normas na <ulink url=\"&url-wiki;DebianIRC\">Wiki Debian</"
"ulink>."

#. type: Content of: <chapter><section><section><para>
#: en/moreinfo.dbk:63
msgid ""
"For more information on OFTC please visit the <ulink url=\"&url-irc-host;"
"\">website</ulink>."
msgstr ""
"Para máis información sobre OFTC visite <ulink url=\"&url-irc-host;\">a súa "
"páxina</ulink>."

#. type: Content of: <chapter><section><title>
#: en/moreinfo.dbk:71
msgid "Reporting bugs"
msgstr "Informar de erros"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:73
msgid ""
"We strive to make Debian a high-quality operating system; however that does "
"not mean that the packages we provide are totally free of bugs.  Consistent "
"with Debian's <quote>open development</quote> philosophy and as a service to "
"our users, we provide all the information on reported bugs at our own Bug "
"Tracking System (BTS).  The BTS can be browsed at <ulink url=\"&url-bts;\"></"
"ulink>."
msgstr ""
"Intentamos facer de Debian un sistema operativo de boa calidade; pero iso "
"non significa que todos os paquetes que distribuímos están totalmente libres "
"de fallos.  En conxunción coa filosofía de <quote>desenvolvemento aberto</"
"quote> de Debian, e coma un servizo para os nosos usuarios, compartimos toda "
"a información sobre os fallos atopados no Sistema de Seguimento de Fallos "
"(BTS en inglés).  O SSF pódese consultar en <ulink url=\"&url-bts;\"></"
"ulink>."

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:81
msgid ""
"If you find a bug in the distribution or in packaged software that is part "
"of it, please report it so that it can be properly fixed for future "
"releases.  Reporting bugs requires a valid e-mail address.  We ask for this "
"so that we can trace bugs and developers can get in contact with submitters "
"should additional information be needed."
msgstr ""
"Se vostede atopa un fallo na distribución, ou nos programas que a forman, "
"informe sobre el para que poida ser arranxado en futuras versións.  Informar "
"de fallos require unha dirección de correo-e válida.  Pedímosllo para que "
"poidamos seguir os fallos e os desenvolvedores se poidan poñer en contacto "
"cos autores do informe para lles requirir información adicional."

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:88
msgid ""
"You can submit a bug report using the program <command>reportbug</command> "
"or manually using e-mail.  You can find out more about the Bug Tracking "
"System and how to use it by reading the reference documentation (available "
"at <filename>/usr/share/doc/debian</filename> if you have <systemitem role="
"\"package\">doc-debian</systemitem> installed) or online at the <ulink url="
"\"&url-bts;\">Bug Tracking System</ulink>."
msgstr ""
"Vostede pode informar dun fallo usando o programa <command>reportbug</"
"command> ou manualmente dende o correo-e.  Pódese atopar máis información "
"sobre o Sistema de Seguimento de Fallos e sobre como usalo lendo a "
"documentación de referencia (dispoñible en <filename>/usr/share/doc/debian</"
"filename> se tes instalado paquete <systemitem role=\"package\">doc-debian</"
"systemitem>) ou na rede no <ulink url=\"&url-bts;\">Sistema de Seguimento de "
"Fallos</ulink>."

#. type: Content of: <chapter><section><title>
#: en/moreinfo.dbk:98
msgid "Contributing to Debian"
msgstr "Colaborar con Debian"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:100
msgid ""
"You do not need to be an expert to contribute to Debian.  By assisting users "
"with problems on the various user support <ulink url=\"&url-debian-list-"
"archives;\">lists</ulink> you are contributing to the community.  "
"Identifying (and also solving) problems related to the development of the "
"distribution by participating on the development <ulink url=\"&url-debian-"
"list-archives;\">lists</ulink> is also extremely helpful.  To maintain "
"Debian's high-quality distribution, <ulink url=\"&url-bts;\">submit bugs</"
"ulink> and help developers track them down and fix them.  The tool "
"<systemitem role=\"package\">how-can-i-help</systemitem> helps you to find "
"suitable reported bugs to work on.  If you have a way with words then you "
"may want to contribute more actively by helping to write <ulink url=\"&url-"
"ddp-vcs-info;\">documentation</ulink> or <ulink url=\"&url-debian-i18n;"
"\">translate</ulink> existing documentation into your own language."
msgstr ""
"Non necesitas ser un experto para colaborar con Debian.  Mesmo axudando aos "
"usuarios cos seus problemas nas diferentes <ulink url=\"&url-debian-list-"
"archives;\">listas</ulink> vostede está contribuíndo á comunidade.  Tamén "
"axuda moito identificar (e resolver) problemas relacionados co "
"desenvolvemento da distribución participando na <ulink url=\"&url-debian-"
"list-archives;\">lista</ulink> de desenvolvemento.  Para manter a calidade "
"da distribución Debian, <ulink url=\"&url-bts;\">informe dos fallos</ulink> "
"e axude aos desenvolvedores a arranxalos.  A ferramenta <systemitem role="
"\"package\">how-can-i-help</systemitem> axudarallee a atopar informes de "
"fallos adecuados nos que traballar.  Se se llee da ben escribir podes "
"contribuír de forma máis activa axudando a escribir <ulink url=\"&url-ddp-"
"vcs-info;\">documentación</ulink> ou <ulink url=\"&url-debian-i18n;"
"\">traducindo</ulink> documentos existentes á súa lingua."

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:117
msgid ""
"If you can dedicate more time, you could manage a piece of the Free Software "
"collection within Debian.  Especially helpful is if people adopt or maintain "
"items that people have requested for inclusion within Debian.  The <ulink "
"url=\"&url-wnpp;\">Work Needing and Prospective Packages database</ulink> "
"details this information.  If you have an interest in specific groups then "
"you may find enjoyment in contributing to some of Debian's <ulink url=\"&url-"
"debian-projects;\">subprojects</ulink> which include ports to particular "
"architectures and <ulink url=\"&url-debian-blends;\">Debian Pure Blends</"
"ulink> for specific user groups, among many others."
msgstr ""
"Se lle chega o tempo poderías xestionar un anaco da colección de Programas "
"Libres dentro de Debian.  Axudaríanos moito se a xente adopta ou mantén "
"elementos que a xente quere que se inclúan con Debian.  A <ulink url=\"&url-"
"wnpp;\">base de datos Fáltanlles Traballo e a base de datos Paquetes "
"Prospectivos</ulink> conteñen esta información.  Se lle interesan grupos "
"específicos entón pode que lle guste contribuír a algún dos <ulink url="
"\"&url-debian-projects;\">subproxectos</ulink> de Debian, que inclúen "
"implementacións a outras arquitecturas e <ulink url=\"&url-debian-blends;"
"\">Mesturas Debian Puras</ulink> para grupos de usuarios específicos, entre "
"outros."

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:128
msgid ""
"In any case, if you are working in the free software community in any way, "
"as a user, programmer, writer, or translator you are already helping the "
"free software effort.  Contributing is rewarding and fun, and as well as "
"allowing you to meet new people it gives you that warm fuzzy feeling inside."
msgstr ""
"Sexa como for, se vostede está traballando na comunidade do software libre "
"de calquera forma, xa for coma usuario, programador, escritor ou tradutor; "
"xa estas axudando a espallar o software libre.  Colaborar é divertido e ten "
"beneficios: ademais de axudarlle a coñecer xente nova poralle bolboretas no "
"bandullo."
